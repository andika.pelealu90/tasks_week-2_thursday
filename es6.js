// var, let, const
// hoisting -

// declaration
var a = 1;
var a = 2;

let b = 1;
let b2 = 1;

const c = 1;
const c2 = 2;

console.log(a, "--var dec");
console.log(b, "--let dec");
console.log(c, "--const dec");

// assignment
a = 3;
b = 3;
// c = 3;

console.log(a, "--var asg");
console.log(b, "--let asg");
// console.log(c, "--const asg");

// declaration & assignment for object & array
let arr1 = [1, 2, 3, 4, 5];
const arr2 = [1, 2, 3, 4, 5];

arr1 = [2, 3, 4];
arr2.push([3, 4, 5]);

console.log(arr1, "--arr1 let");
console.log(arr2, "--arr2 const");

// let console = {
//   log: "print",
//   error: "log error",
// };

// hoisting
h = 5;
var h;

console.log(h, "--hoisting");

// arrow function

// function declaration
function sum() {
  return a + b;
}

// console.log(sum(1, 2), "--function sum");

// function expression
let sum1 = function (a, b) {
  return a + b;
};

// arrow itu nulisnya expression
// nama
// parameter || no parameter
// statement
// return

let sumArrow1 = (a, b = 0) => {
  let c = 100;

  let average = (a + b + c) / 3;

  return average;
};

let sumArrow2 = () => 2;

console.log(sumArrow1(1), "---arrow 1");
console.log(sumArrow2(1, 3), "---arrow 2");

// ternary operator || saat kita punya 2 pilihan
h == 5 ? (true ? console.log("lima") : console.log("bukan lima")) : false;

class Person {
  constructor(name, age, atmCard) {
    this.name = name; // variable > property
    this.age = age;
    this.country = "Indonesia";
    this.atmCard = atmCard;
    this.greet = this.greet;
  }

  greet() {
    console.log("Selamat pagi"); // function > method
  }

  static createUser(name, age) {}

  static getUser(id) {}

  postStory() {}

  likePost() {}
}

const person1 = new Person("Idrus", 17, 13175376215);

console.log(person1, "--person 1");
person1.greet();

const person2 = {
  name: "Bobby",
  age: 18,
  gender: "Male",
};
