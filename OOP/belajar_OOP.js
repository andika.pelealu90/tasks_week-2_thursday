// Buatlah sebuah Class Student, yang memiliki atribut berikut:
// ● Name,
// ● Age,
// ● Date of Birth,
// ● Gender
// ● Student ID (bisa berupa angka atau teks), dan
// ● Hobbies (bisa menampung lebih dari 1 hobi).
// Class tersebut juga bisa memanggil fungsi dengan proses sebagai berikut:
// ● SetName: mengubah nama student dengan mengirimkan satu parameter ke dalam fungsi berupa teks
// ● SetAge: mengubah umur student dengan mengirimkan satu parameter ke dalam fungsi berupa angka
// ● SetDateOfBirth: mengubah tanggal lahir student dengan mengirimkan satu parameter ke dalam fungsi berupa teks
// ● SetGender: mengubah gender student dengan mengirimkan satu parameter ke dalam fungsi berupa teks, dan hanya
// bisa menerima nilai Male atau Female
// ● addHobby: menambah hobi dengan mengirimkan satu parameter ke dalam fungsi berupa teks
// ● removeHobby: menghapus list hobi yang ada dengan mengirimkan satu parameter berupa teks, yang merupakan hobi
// apa yang akan dihapus
// ● getData: menampilkan seluruh data atribut murid

class Student {
    constructor (name,age,DoB,gender,id,hobbies){
        this._name = name;
        if (age === Number) {
            this._age = age;
        }
        this._DateOfBirth = DoB;
        if (gender === "Female" || gender === "Male") {
            this._gender = gender;
        } else {"pick legit gender"}
        this.StudentId = id;
        this.hobby = [hobbies];
    }
    DeleteHobby (del) {
        for (let i = 0; i < this.hobby.length; i++) {
            if (this.hobby[i] === `${del}`) {
                delete this.hobby[i]
                console.log(`hobby ${del} deleted`);
                break ;
            } 
        }
    }
    AddHobby (newHobby){
        this.hobby.push(newHobby)
        return "Hobby added"
    }
    SetName(newName) {
        this._name = newName;
        return "Update Success"
    }
    SetGender(newGender) {
        if (newGender === "Female" || newGender === "Male") {
            this._gender = newGender;
            return "Update Success"

        }return "pick legit gender"
    }
    SetDoB(newDoB) {
        this._DateOfBirth = newDoB;
        return "Update Success"
    }
    SetDateOfBirth(newAge){
        this._age = newAge
        return "Update Success"
    }
    Details(name){
        console.log(name)    }
}
rangga = new Student("Rangga",28,"30 Feb 2000","Male",34234234,['travelling','jogging'])
rangga.Details(rangga)
rangga.AddHobby('makan')
console.log(rangga);
rangga.DeleteHobby("makan")
console.log(rangga.SetGender("mokumoku"))
rangga.Details(rangga)
