// 1. Convert Minute to time
// ```javascript
    // function convertMinute(second){
    //     let temp = ""
    //     let tmp =""
    //     let menit = Math.floor(second/60)
    //     let detik = `${second%60}`
    //     temp += `${menit}:`
    //     detik.length < 2 ? temp += `0${detik}` : temp += `${detik}`
    //     return temp

    // }

    convertMinute = (sec) => {
        let temp = ""
        let menit = Math.floor(sec/60)
        let detik = `${sec%60}`
        temp += `${menit}:`
        detik.length < 2 ? temp += `0${detik}` : temp += `${detik}`
        return temp
    }

//     //Test Case
    console.log(convertMinute(100)) //1:40
    console.log(convertMinute(185)) //3:05
